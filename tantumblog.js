function loadBlog() {
  var footerString="";
  var postCounter = 0;

  const renderer = new marked.Renderer();

  // Override function
  renderer.heading = function (text, level) {
    const escapedText = text.toLowerCase().replace(/[^\w]+/g, '-');
    var elementString = "";

    if (level === 1) {
      if (postCounter > 0) {
        elementString += `<footer>${footerString}</footer>`
        elementString += "</article>"
      }


      elementString += `
    <article id="${escapedText}">
    <h${level}>
      <a name="${escapedText}" class="anchor" href="#${escapedText}">
      </a>
      ${text}
    </h${level}>`;
    
      footerString=`<a href="${window.location}#${escapedText}">Direct link</a>`;
      postCounter++;
    } else {
      elementString += `
    <h${level}>
      ${text}
    </h${level}>`;
    }
    return elementString;
  };

  marked.renderer = renderer;

  $("#blogMain").html("<p><em>Loading....please wait<em></p>");
  $.get("blogposts.md", function (data) {
    var rawhtml = marked.parse(data, { renderer: renderer });
    if (postCounter > 0) {
      $("#blogMain").append(`<footer>${footerString}</footer>`);
      $("#blogMain").append("</article>");
    }
    var cleanhtml = DOMPurify.sanitize(rawhtml);
    $("#blogMain").html(cleanhtml);
    console.log($("#blogMain").html());
  }).fail(function () {
    $("#blogMain").html("<p><strong>Error loading the posts data file!</strong></p>");
  });

}

