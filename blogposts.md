# tantumblog - a simple tech blog
Date: 12/24/2018  
## Keep it simple and basic
**Simple** hosting, only *static file hosting* is needed, like with github or bitbucket. No server side code execution or database is needed.  
**Simple** writing, just write your posts in a single text file.  
**Basic** formating, use [markdown](https://daringfireball.net/projects/markdown/) for post formating.  
**Basic** functionality, provide a basic blog. 

# tantumblog development starts
Date: 12/26/2018  
Development started with plain javascript for a first tryout. For markdown parsing [markedJS](https://marked.js.org/#/README.md#README.md) is used. 

# tantumblog development continues
Date: 01/04/2020  
Reviewed decision from the start of the project.  
Removed npm package manager instead using link to cdn [jsdelivr](https://www.jsdelivr.com).

# updated inital use case diagramm
Date: 01/05/2020  
<img src="out/design/usecases/ucblogger/tantumblog use case.png" width=600/> 

# html5 syntax of a blog post
Date: 01/06/2020  

Basic html5 syntax of a blogpost could look like this:

        <!-- Article begins with new header 1-->
        <article>
            <header>
                <h1>title</h1>
                <p>date</p>
            </header>
            <p>content</p>
            <footer>
                <p>direkt link</p>
                <p>tags</p>
            </footer>
        </article>




